
image:
	docker build -t kuriyama/ubuntu-ssh-agent:20.04 -f Dockerfile.20.04 .

pull:
	@docker pull ubuntu:20.04 > .log-20.04
	@docker image inspect ubuntu:20.04 | jq -r '.[0].Id' > .id-20.04

check:
	@if git status -s | grep -q M; then\
		git add .etag .id-20.04 && git commit -m "Update IDs."; git push;\
	fi

etag:
	@curl -s https://packages.ubuntu.com/focal/openssh-client | grep Keywords | sed -e 's|.*content="||; s|">||; s|Ubuntu, *||' > .etag
